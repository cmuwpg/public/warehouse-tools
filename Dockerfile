FROM pypy:3.6-slim-buster

ENV ZK_SHELL_VERSION 1.3.1
ARG MC_VERSION=RELEASE.2020-08-08T02-33-58Z
ARG CLI53_VERSION=0.8.17
ARG KAFKA_VERSION=2.8.1
ENV DEBIAN_FRONTEND noninteractive

RUN mkdir -p /usr/share/man/man1

RUN apt-get -qq update && apt-get -qq -y --no-install-recommends install \
    gcc default-jdk curl jq restic openssh-client

RUN pip install -U zk-shell==$ZK_SHELL_VERSION

ADD https://dl.min.io/client/mc/release/linux-amd64/archive/mc.$MC_VERSION /usr/local/bin/mc
RUN chmod 0755 /usr/local/bin/mc

RUN mkdir -p /opt/kafka \
    && curl -L -o /opt/kafka/kafka.tgz http://apache.mirror.vexxhost.com/kafka/$KAFKA_VERSION/kafka_2.13-$KAFKA_VERSION.tgz \
    && tar -C /opt/kafka --strip-components=1 -xzf /opt/kafka/kafka.tgz \
    && rm /opt/kafka/kafka.tgz

RUN curl -L -o /usr/local/bin/cli53 https://github.com/barnybug/cli53/releases/download/$CLI53_VERSION/cli53-linux-amd64 \
    && chmod +x /usr/local/bin/cli53

CMD ["sleep", "infinity"]
